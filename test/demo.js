const Joi = require('joi')
const should = require('should')
const util = require('util')

function show (stuff) {
  console.log(util.inspect(stuff, { depth: 99 }))
}

const stringSchema = Joi.string()
const aString = 'this is a string'
const numberSchema = Joi.number()
const aNumber = 4

// https://joi.dev/api/?v=17.4.0

describe('usage', function () {
  describe('validate', function () {
    it('validates ok', function () {
      const validation = stringSchema.validate(aString)
      show(validation)
      validation.value.should.equal(aString)
      should(validation.error).be.undefined()
    })
    it('validates nok', function () {
      const stringSchema = Joi.string()
      const validation = stringSchema.validate(aNumber)
      show(validation)
      validation.value.should.equal(aNumber)
      validation.should.be.an.Object()
    })
  })

  describe('asserts (throws)', function () {
    it('asserts ok', function () {
      Joi.assert(aString, stringSchema)
    })
    it('asserts nok', function () {
      const error = (() => {
        Joi.assert(aNumber, stringSchema)
      }).should.throw()
      show(error)
    })
  })

  describe('attempts (throws)', function () {
    it('asserts ok', function () {
      const result = Joi.attempt(aString, stringSchema)
      show(result)
      result.should.equal(aString)
    })
    it('asserts nok', function () {
      const error = (() => {
        Joi.attempt(aNumber, stringSchema)
      }).should.throw()
      show(error)
    })
  })

  describe('meta', function () {
    it('is a schema', function () {
      Joi.isSchema(stringSchema).should.be.true()
    })
  })
})
describe('any', function () {
  const validSchema = Joi.any().valid('yellow', 'green', 4)
  it('valid', function () {
    Joi.assert('yellow', validSchema)
    Joi.assert(4, validSchema)
    const error = (() => {
      Joi.assert('brown', validSchema)
    }).should.throw()
    show(error)
  })
  it('allow', function () {
    const allowSchema = stringSchema.allow(4)
    Joi.assert(aString, allowSchema)
    Joi.assert(4, allowSchema)
    const error = (() => {
      Joi.assert(7, allowSchema)
    }).should.throw()
    show(error)
  })
  it('concat (and)', function () {
    const allowSchema = stringSchema.concat(validSchema)
    Joi.assert('yellow', allowSchema)
    Joi.assert(4, allowSchema)
    const error = (() => {
      Joi.assert(aString, allowSchema)
    }).should.throw()
    show(error)
  })
  it('invalid', function () {
    const invalidString = 'invalid string'
    const invalidSchema = stringSchema.invalid(invalidString)
    Joi.assert(aString, invalidSchema)
    const error = (() => {
      Joi.assert(invalidString, invalidSchema)
    }).should.throw()
    show(error)
  })
})
describe('alternatives', function () {
  it('accepts alternatives', function () {
    const alternativeSchema = Joi.alternatives().try(stringSchema, numberSchema)
    Joi.assert('yellow', alternativeSchema)
    Joi.assert(4, alternativeSchema)
    const error = (() => {
      Joi.assert(true, alternativeSchema)
    }).should.throw()
    show(error)
  })
})
describe('string', function () {
  const alphanum = stringSchema.alphanum()
  it('alphanum', function () {
    Joi.assert('azAZ09', alphanum)
    const error = (() => {
      Joi.assert(aString, alphanum)
    }).should.throw()
    show(error)
  })
  it('pattern', function () {
    const Crn = Joi.string().pattern(/^\d{4}\.\d{3}\.\d{3}$/)
    Joi.assert('0123.456.789', Crn)
    const error = (() => {
      Joi.assert('0123456789', Crn)
    }).should.throw()
    show(error)
  })
  it('trim', function () {
    const trimmed = 'this string is trimmed'
    const untrimmed = ' \t   this string is not trimmed   '
    const trimmedSchema = Joi.string().trim()
    Joi.assert(trimmed, trimmedSchema)
    const result1 = Joi.attempt(trimmed, trimmedSchema)
    show(result1)
    result1.should.equal(trimmed)
    const result2 = Joi.attempt(untrimmed, trimmedSchema)
    show(result2)
    result1.should.equal(trimmed)
    const error = (() => {
      Joi.assert(untrimmed, trimmedSchema, { convert: false })
    }).should.throw()
    show(error)
  })
  /*
base64()
case()
creditCard()
dataUri()
domain()
email()
guid() - aliases:
uuid
hex()
hostname()
insensitive()
ip()
isoDate()
isoDuration()
length()
lowercase()
max()
min()
normalize()
regex
replace()
token()
truncate()
uppercase()
uri()
   */
})
describe('number', function () {
  /*
greater()
integer()
less()
max()
min()
multiple()
negative()
port()
positive()
precision()
sign()
unsafe()
   */
})
describe('boolean', function () {
  /*
falsy()
sensitive()
truthy()
   */
})
describe('date', function () {
  /*
greater()
iso()
less()
max()
min()
timestamp()
   */
})
describe('function', function () {
  /*
function.arity()
function.class()
function.maxArity()
function.minArity()
   */
})
describe('object', function () {
  it('validates objects', function () {
    const objectSchema = Joi.object()
    Joi.assert({}, objectSchema)
    Joi.assert({ aProperty: 4 }, objectSchema)
    Joi.assert(new Date(), objectSchema)
    const error1 = (() => {
      Joi.assert(aString, objectSchema)
    }).should.throw()
    show(error1)
    const error2 = (() => {
      Joi.assert([1, 2, 3, 4], objectSchema)
    }).should.throw()
    show(error2)
    const error3 = (() => {
      Joi.assert(() => 4, objectSchema)
    }).should.throw()
    show(error3)
  })
  /*
and()
append()
assert()
instance()
keys()
length()
max()
min()
nand()
or()
oxor()
pattern()
ref()
regex()
rename()
schema()
unknown()
with()
without()
xor()
   */
})
describe('array', function () {
  /*
has()
items()
length()
max()
min()
ordered()
single()
sort()
sparse()
unique()
   */
})
describe('binary', function () {
  /*
encoding()
length()
max()
min()
   */
})
describe('more', function () {
  describe('reflection', function () {
    it('knows its type', function () {
      show(stringSchema.type)
    })
  })
  /*
  Joi.extend
  when
  tailor/alter
  custom
  default
  describe
  example
  label
  unit
  meta
  note
  external
  extract
 */
})
